class WC:
    f = None
    
    def __init__(self, namafile):
        self.txt = namafile
       

    def word_count(self):
        self.f = open(self.txt,'rt') 
        word = 0  
        for line in self.f : 
            word += len(line.split(" "))
        self.f.close()
        return word
        

    def line_count(self):
        self.f = open(self.txt,'rt') 
        lines = 0  
        for line in self.f : 
            lines += 1
        self.f.close()
        return lines

    def char_count(self):
        self.f = open(self.txt,'rt') 
        character = 0  
        for line in self.f : 
            character += len(line)
        self.f.close()
        return character